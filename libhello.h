#ifndef LIBHELLO_H
#define LIBHELLO_H
/* libhello.h - library pulic functions */

/* demo function provided by libhello - returns x+y */
extern int libhello_add(int x, int y);

#endif
