/* testhello.c - test libhello library */

#include<stdio.h>
#include<stdlib.h>

#include "libhello.h"



int main(int argc, char **argv)
{
	int exp_res = 8;
	int res = libhello_add(5,3);
	if ( res != exp_res ){
		fprintf(stderr,"libhello_add(5,3) failed: got %d, expected %d\n", res, exp_res);
		return EXIT_FAILURE;
	}
	printf("Test of libhello_add() finished OK\n");
	return EXIT_SUCCESS;
}

