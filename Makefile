# GNU-Makefile for hello-lib project

# allow out-of-tree builds
VPATH ?= .

LIBHELLO_MAJOR := 1
LIBHELLO_MINOR := 0
LIBHELLO_PATCH := 0

LIB_NAME   := libhello
LIB_SOBASE := $(LIB_NAME).so
LIB_SONAME := $(LIB_NAME).so.$(LIBHELLO_MAJOR)
LIB_SO     := $(LIB_NAME).so.$(LIBHELLO_MAJOR).$(LIBHELLO_MINOR).$(LIBHELLO_PATCH)
LIB_CFLAGS := -Wall -shared -Wl,-soname,$(LIB_SONAME)
TEST_CFLAGS := -Wall

all : $(LIB_SO) testhello

$(LIB_SO) : $(LIB_NAME).c $(LIB_NAME).h
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LIB_CFLAGS) $< -o $@

$(LIB_SOBASE) : $(LIB_SO)
	rm -f $@
	ln -s $< $@

$(LIB_SONAME) : $(LIB_SO)
	rm -f $@
	ln -s $< $@

TEST_LDFLAGS := -L.
TEST_LDLIBS  := -lhello
testhello : testhello.c libhello.h $(LIB_SO) $(LIB_SOBASE) $(LIB_SONAME)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(TEST_CFLAGS) $(LDFLAGS) $(TEST_LDFLAGS) $(TARGET_ARCH) $< $(LOADLIBES) $(TEST_LDLIBS) -o $@

.PHONY: all clean
clean:
	rm -f -- *.so* testhello

